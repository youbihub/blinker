#include <blinker/blinker.h>
#include <blinker/version.h>
#include <doctest/doctest.h>

#include <iostream>
#include <string>

// #include "bayes/bayes.h"

TEST_CASE("Crenel function") {
  using namespace blinker;
  SUBCASE("No phase") {
    auto crenel = Crenel{.period = 0.1, .alpha = 0.25};
    auto phase_rad = 0;
    CHECK_EQ(crenel(phase_rad, 0), true);
    CHECK_EQ(crenel(phase_rad, 0.01), true);
    CHECK_EQ(crenel(phase_rad, 0.025), true);
    CHECK_EQ(crenel(phase_rad, 0.026), false);
    CHECK_EQ(crenel(phase_rad, 0.099), false);
    // after N cycles
    auto n = 123456;
    CHECK_EQ(crenel(phase_rad, n * 0.1 + 0), true);
    CHECK_EQ(crenel(phase_rad, n * 0.1 + 0.01), true);
    CHECK_EQ(crenel(phase_rad, n * 0.1 + 0.025), true);
    CHECK_EQ(crenel(phase_rad, n * 0.1 + 0.026), false);
    CHECK_EQ(crenel(phase_rad, n * 0.1 + 0.099), false);
  }
  SUBCASE("Some phase") {
    auto crenel = Crenel{.period = 0.1, .alpha = 0.25};
    auto phase_rad = 1.23;
    auto dt = phase_rad / 2 / std::numbers::pi * crenel.period;
    CHECK_EQ(crenel(phase_rad, -dt + 0), true);
    CHECK_EQ(crenel(phase_rad, -dt + 0.01), true);
    CHECK_EQ(crenel(phase_rad, -dt + 0.025), true);
    CHECK_EQ(crenel(phase_rad, -dt + 0.026), false);
    CHECK_EQ(crenel(phase_rad, -dt + 0.099), false);
    // after N cycles
    auto n = 123456;
    CHECK_EQ(crenel(phase_rad, -dt + n * 0.1 + 0), true);
    CHECK_EQ(crenel(phase_rad, -dt + n * 0.1 + 0.01), true);
    CHECK_EQ(crenel(phase_rad, -dt + n * 0.1 + 0.025), true);
    CHECK_EQ(crenel(phase_rad, -dt + n * 0.1 + 0.026), false);
    CHECK_EQ(crenel(phase_rad, -dt + n * 0.1 + 0.099), false);
  }
}

TEST_CASE("Blinker version") {
  static_assert(std::string_view(BLINKER_VERSION) == std::string_view("1.0"));
  CHECK(std::string(BLINKER_VERSION) == std::string("1.0"));
}
