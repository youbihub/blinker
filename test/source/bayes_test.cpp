#include "bayes/bayes.h"

#include <optional>

#include "blinker/blinker.h"
#include "doctest/doctest.h"

TEST_CASE("bayes") {
  auto dyn = blinker::ActiveDynamics{.p_on_off = 0.,
                                     .p_on_cli = 0.,
                                     .p_off_on = 0.,
                                     .p_off_cli = 0.,
                                     .p_cli_on = 0.,
                                     .p_cli_off = 0.,
                                     .phase_std = 0.};
  auto obs = blinker::ActiveObserver{};
  auto inferer = bayes::Post<blinker::ActiveDynamics, blinker::ActiveObserver>{dyn, obs};
  auto density = blinker::ProbDensity(12, 0.6, 0.3, blinker::Crenel{.period = 1.2, .alpha = 0.6});
  auto observation
      = std::make_optional(blinker::ActiveScore{.active = true, .score = 0.8, .time = 123.23});

  auto output = inferer(density, observation);
}