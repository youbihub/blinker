DOXYFILE = 'Doxyfile'

LINKS_NAVBAR1 = [
    (None, 'pages', [(None, 'about')]),
    (None, 'namespaces', []),
]

# Add your own navbar links using the code below.
# To find the valid link names, you can inspect the URL of a generated documentation site.

# LINKS_NAVBAR1 = [
#     (None, 'pages', [(None, 'about')]),
#     (None, 'namespaces', [(None, 'namespaceblinker')]),
# ]
#
# LINKS_NAVBAR2 = [
#     (None, 'annotated', [(None, 'classblinker_1_1_blinker')]),
#     (None, 'files', [(None, 'blinker_8h')]),
# ]
