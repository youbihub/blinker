#include <iostream>
#include <optional>

// #include "Eigen/Core"
// #include "Eigen/Dense"
// #include "Eigen/src/Core/ArithmeticSequence.h"
#include "bayes/bayes.h"
#include "blinker/blinker.h"

auto print(const Eigen::ArrayXd& q) {
  auto p = q.transpose();
  std::cout << p(Eigen::seqN(0, p.size() - 2)) << " | "
            << p(Eigen::seq(Eigen::last - 1, Eigen::last)) << std::endl;
}

auto main(int /* argc */, char** /* argv */) -> int {
  auto dyn = blinker::ActiveDynamics{.p_on_off = 0.01,
                                     .p_on_cli = 0.01,
                                     .p_off_on = 0.01,
                                     .p_off_cli = 0.01,
                                     .p_cli_on = 0.01,
                                     .p_cli_off = 0.01,
                                     .phase_std = 3.14 / 10};
  auto obs = blinker::ActiveObserver{};
  auto inferer = bayes::Post<blinker::ActiveDynamics, blinker::ActiveObserver>{dyn, obs};
  auto density
      = blinker::ProbDensity(12, 1. / 3, 1. / 3, blinker::Crenel{.period = 1.2, .alpha = 0.6});
  auto time = 123.23;
  auto observation
      = std::make_optional(blinker::ActiveScore{.active = true, .score = 0.9, .time = time});

  print(density.p);
  for (int i = 0; i < 24; i++) {
    time += 0.1;
    if (i % 2 == 0) {
      density = inferer(density);
    } else {
      observation
          = blinker::ActiveScore{.active = density.crenel(3., time), .score = 0.9, .time = time};
      density = inferer(density, observation);
    }
    print(density.p);
  }
  return 0;
}
