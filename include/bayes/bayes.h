#pragma once

#include <algorithm>
#include <functional>
#include <optional>
#include <ranges>

namespace bayes {

  template <typename Dynamics, typename Observer> struct Post {
    auto operator()(const auto& density,
                    const std::optional<typename Observer::Obs>& obs = std::nullopt) {
      auto est = dynamics(density);
      if (obs) {
        est = observer(obs.value(), est) * est;
      }
      return est.norm();
    };
    Dynamics dynamics;
    Observer observer;
  };
}  // namespace bayes