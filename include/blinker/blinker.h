#pragma once

#include <Eigen/Core>
#include <numbers>
#include <optional>

namespace blinker {

  struct ActiveScore {
    bool active;
    double score;
    double time;
  };

  // créneau de ref
  struct Crenel {
    double period;
    double alpha;
    // double phase_rad;
    auto operator()(const double phase_rad, const double t) const {
      auto tu = t + phase_rad / 2 / std::numbers::pi * period;
      tu = tu - std::floor(tu / period) * period;
      if (tu / period <= alpha)
        return true;
      else {
        return false;
      }
    }
  };

  struct ProbDensity {
    ProbDensity(const int n_phase, const double prior_on, const double prior_off, const Crenel& cr)
        : crenel(cr) {
      assert(prior_on + prior_off <= 1);
      const auto prior_blink = 1. - prior_off - prior_on;
      p = Eigen::ArrayXd(n_phase + 2);
      p_on() = prior_on;
      p_off() = prior_off;
      p_phase().setConstant(prior_blink / n_phase);
      phase = Eigen::ArrayXd(n_phase);
      phase.setLinSpaced(0, 2 * std::numbers::pi / n_phase * (n_phase - 1));
    }
    auto p_on() -> double& { return p(p.size() - 2); }
    auto p_on() const -> double { return p(p.size() - 2); }
    auto p_off() -> double& { return p(p.size() - 1); }
    auto p_off() const -> double { return p(p.size() - 1); }
    auto p_phase() -> Eigen::VectorBlock<Eigen::ArrayXd> { return p(Eigen::seqN(0, p.size() - 2)); }
    auto p_phase() const -> Eigen::ArrayXd { return p(Eigen::seqN(0, p.size() - 2)); }
    auto p_cli() const { return p_phase().sum(); }
    auto operator*(const ProbDensity& other) {
      auto y = *this;
      y.p *= other.p;
      return y;
    }
    auto norm() {
      p /= p.sum();
      return *this;
    }
    Eigen::ArrayXd p;
    Eigen::ArrayXd phase;
    Crenel crenel;
  };

  struct ActiveObserver {
    auto operator()(const ActiveScore& obs, const ProbDensity& prob) {
      auto est = prob;
      auto model_on = est.phase.unaryExpr([&](const auto& p) { return est.crenel(p, obs.time); });
      auto arrscore = Eigen::ArrayXd(prob.phase.size());
      arrscore.setConstant(obs.score);
      if (obs.active) {  // todo: could be "more clever"
        est.p_on() = obs.score;
        est.p_off() = 1. - obs.score;
        est.p_phase() = model_on.select(arrscore, 1. - arrscore);
      } else {
        est.p_on() = 1. - obs.score;
        est.p_off() = obs.score;
        est.p_phase() = model_on.select(1. - arrscore, arrscore);
      }
      return est;
    }
    using Obs = ActiveScore;
  };

  struct ActiveDynamics {
    auto p_ph1_ph2(const double ph1, const double ph2) {
      auto dp = std::numbers::pi - ph1;
      auto p1 = ph1 + dp;
      auto p2 = ph2 + dp;
      p2 -= std::floor(p2 / 2. / std::numbers::pi) * 2 * std::numbers::pi;
      return std::exp(-std::pow(p1 - p2, 2) / 2 / std::pow(phase_std, 2));
    }
    auto operator()(const ProbDensity& prob) {
      auto est = prob;
      est.p.setZero();
      // on-propagation
      auto p_on_on = 1. - p_on_cli - p_on_off;
      est.p_off() += prob.p_on() * p_on_off;
      est.p_phase() += prob.p_on() * p_on_cli / prob.phase.size();
      est.p_on() += prob.p_on() * p_on_on;
      // off-propagation
      auto p_off_off = 1. - p_off_cli - p_off_on;
      est.p_on() += prob.p_off() * p_off_on;
      est.p_phase() += prob.p_off() * p_off_cli / prob.phase.size();
      est.p_off() += prob.p_off() * p_off_off;
      // cli-propagation
      auto p_cli_cli = 1. - p_cli_off - p_cli_on;
      est.p_on() += prob.p_cli() * p_cli_on;
      est.p_off() += prob.p_cli() * p_cli_off;
      for (int i = 0; i < prob.phase.size(); i++) {
        auto temp = Eigen::ArrayXd::Zero(prob.phase.size()).eval();
        for (int j = 0; j < est.phase.size(); j++) {
          temp(j) = p_ph1_ph2(prob.phase(i), est.phase(j));
        }
        est.p_phase() += prob.p_phase()(i) * temp / temp.sum() * p_cli_cli;
      }
      return est;
    }

    double p_on_off;
    double p_on_cli;
    double p_off_on;
    double p_off_cli;
    double p_cli_on;
    double p_cli_off;
    double phase_std;
  };

  // state = bay_update(observer,dynamics,state,data)
  // state = bay_update(state,data)

  // p(y|k), p(x_k+1|x_k)

}  // namespace blinker